// Filename: users-routes.js
// Users API routes

let router = require('express').Router();

var users = require('../controllers/users-controller')

router.get('/', function (req, res) {
    users.list(req, res)
});

router.get('/:username/details', function (req, res) {
    users.details(req, res)
})

router.get('/:username/repos', function (req, res) {
    users.repos(req, res)
})

// Export API routes
module.exports = router