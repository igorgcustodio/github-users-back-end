# GitHub Users API

This project is a "proxy" for [Github API](https://developer.github.com/v3/)

The main purpose is to practice and learning Node.JS

## Routes

- **[GET]** /api/users?since={number}
- **[GET]** /api/users/:username/details
- **[GET]** /api/users/:username/repos

## Dependencies

### Development

- Nodemon @ 1.18.3

### Production

- Body-Parser @ 1.18.3
- Express @ 4.16.3
- Request @ 2.87.0

