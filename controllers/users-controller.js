let request = require('request')
let config = require('../config/config')

let headers = {
    'User-Agent': config.params.USER
}

// API: /users?since={number}
// Return the list of users
exports.list = function(req, res) {

    if (req.query != null)
        var url = config.params.BASE_URL + 'users'
    else
        var url = config.params.BASE_URL + 'users?since=' + req.query.since

    var options = {
        url: url,
        headers: headers,
        json: true
    }

    var callback = function(error, response, body){
        var status = 0
        var message = ''
        if (error != null) {
            status = response.statusCode
            message = 'Some error has occurred'
        } else {
            status = response.statusCode

            var users = []

            body.forEach(user => {                
                var newUser = user
                newUser.link = 'http://localhost/api/users/' + user.login + '/details'
                users.push(newUser)
            });

            message = users
        }
        res.json({
            status: status,
            message: message,
            error: error
        })
    }

    request(options, callback)    
}

// API: /users/:username/details
// Return the details of the selected user
exports.details = function(req, res) {

    let url = config.params.BASE_URL + 'users/' + req.params.username

    var options = {
        url: url,
        headers: headers,
        json: true
    }

    var callback = function(error, response, body){
        var status = 0
        var message = ''
        if (error != null) {
            status = 404
            message = 'Some error has occurred'
        } else {
            status = 200
            message = body
        }
        res.json({
            status: status,
            message: message,
            error: error
        })
    }

    request(options, callback)
}

// API: /users/:username/repos
// Return the repositories of the user
exports.repos = function(req, res) {

    let url = config.params.BASE_URL + 'users/' + req.params.username + '/repos'

    var options = {
        url: url,
        headers: headers,
        json: true
    }

    var callback = function(error, response, body){
        var status = 0
        var message = ''
        if (error != null) {
            status = 404
            message = 'Some error has occurred'
        } else {
            status = 200
            message = body
        }
        res.json({
            status: status,
            message: message,
            error: error
        })
    }

    request(options, callback)
}

// API: /
// Default route
exports.home = function(req, res) {
    res.json({
        status: 200,
        message: "Its working!"
    })
}