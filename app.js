// Filename: app.js
// Application entry point

let express = require('express')
let bodyParser = require('body-parser')
let apiRoutes = require("./routes/users-routes")

let app = express()
let port = process.env.PORT || 3000

app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

app.get('/', (req, res) => res.send('Application'))

app.use('/api/users', apiRoutes)

app.listen(port, function() {
  console.log('Application started on: ' + port)
})